<?php

namespace App\Http\Controllers;

use App\Models\Bay;
use App\Models\Booking;
use App\Models\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BookingController extends Controller
{
    public function book(Request $request){
        $userid = \Auth::user()->id;
        // Check user booked
        $hasuserbook = Booking::where('user_id', $userid)->whereNull('paid')->first();

        if($hasuserbook){ //has user booked
            return false;
        }else{
            $updatebay = Bay::where('id', $request->id)->update(['booked' => date('Y-m-d H:i:s')]);

            $insertbook = Booking::insert(['user_id' => $userid, 'bay_id' => $request->id, 'start_time' =>  date('Y-m-d H:i:s')]);

            if($updatebay && $insertbook){
                return 'succses';
            }else{
                Bay::where('id', $request->id)->update(['booked' => null]);
                return false;
            }
        }


    }

    public function checkpayment(){
        $booked = Booking::where('user_id', \Auth::user()->id)->whereNull('paid')->first();
        $duration = $this->checkduration($booked->start_time);
        $price = Price::where('mintime','<=',$duration)->where('maxtime','>=',$duration)->first();

        return $price;
    }

    public function pay(Request $request){
        $price = $this->checkpayment();

        $insertbook = Booking::where('id', $request->id)->update(['price_id' => $price->id, 'paid' => Carbon::now()]);

        $booking = Booking::find($request->id);

        if($insertbook){
            $updatebay = Bay::where('id', $booking->bay_id)->update(['booked' => NULL]);
            return 'succses';
        }else{
            Bay::where('id', $booking->bay_id)->update(['booked' => $booking->start_time]);
            return false;
        }
    }

    public function checkduration($start_time){
        $startTime  = Carbon::parse($start_time);
        $finishTime = Carbon::now();
        $duration   = ($finishTime->diffInMinutes($startTime)) / 60;

        return $duration;
    }
}
