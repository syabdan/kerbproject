<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bay;
use App\Models\Booking;

class DashboardController extends Controller
{
    public function index(){
        $bays = Bay::paginate(9);
        $booked = Booking::where('user_id', \Auth::user()->id)->whereNull('paid')->first();

        return view('dashboard', ['bays' => $bays, 'booked' => $booked]);
    }
}
