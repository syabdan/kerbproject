<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert([
            'detail' => '0 - 1 hour',
            'mintime' => 0,
            'maxtime' => 1,
            'price' => 0
        ]);

        DB::table('prices')->insert([
            'detail' => '1 - 2 hour',
            'mintime' => 1.0001,
            'maxtime' => 2,
            'price' => 20
        ]);

        DB::table('prices')->insert([
            'detail' => '2 - 3 hour',
            'mintime' => 2.0001,
            'maxtime' => 3,
            'price' => 60
        ]);

        DB::table('prices')->insert([
            'detail' => '3 - 4 hour',
            'mintime' => 3.0001,
            'maxtime' => 4,
            'price' => 240
        ]);

        DB::table('prices')->insert([
            'detail' => '> 4 hour',
            'mintime' => 4.0001,
            'maxtime' => 8766,
            'price' => 300
        ]);
    }
}
