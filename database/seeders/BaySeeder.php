<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bays')->insert([
            'name' => 'Bay 1',
        ]);

        DB::table('bays')->insert([
            'name' => 'Bay 2',
        ]);

        DB::table('bays')->insert([
            'name' => 'Bay 3',
        ]);
    }
}
