<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="row">
                    @foreach ($bays as $bay)
                    <div class="col-sm-6 col-md-4">
                        <div class="card border-white">
                            <div class="card-header text-center">{{ $bay->name }}</div>
                            <div class="card-body text-center">

                               @if($bay->booked)
                                    @if (!$booked)
                                        <img src="{{ url('assets/img/kerb.png') }}" alt="" width="200px" style="margin: 0 auto;"><br>
                                    @endif

                                    <br><span class="bg bg-danger text-white p-2">Occupaid</span><br>
                                    @if($booked)
                                        <div class="text-center m-5 text-xl">
                                            <label id="minutes" class="text-center">00</label>:<label id="seconds">00</label><br>
                                        </div>
                                        <button class="btn btn-outline-danger mt-4" id="pay" data-id="{{ $booked->id }}">Pay Now</button>
                                    @endif
                                @else
                                    <img src="{{ url('assets/img/kerb.png') }}" alt="" width="200px" style="margin: 0 auto;"><br>
                                    <span class="bg bg-success text-white p-2 mt-5">Available Space</span><br>
                                    @if(!$booked)
                                        <button class="btn btn-outline-success mt-4" id="booking" data-id="{{ $bay->id }}">Book Now</button>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    var minutesLabel = document.getElementById("minutes");
    var secondsLabel = document.getElementById("seconds");
    var totalSeconds = 0;
    setInterval(setTime, 1000);

    function setTime() {
        ++totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds % 60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
    }

    function pad(val) {
        var valString = val + "";
        if (valString.length < 2) {
            return "0" + valString;
        } else {
            return valString;
        }
    }

    $(document).on("click","#booking",function() {
        var id = $(this).attr('data-id');

        swal({
            title: "Are you sure book this parking bay?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EB0000",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "{{ url('booking') }}",
                data: {'id': id, "_token": "{{ csrf_token() }}"},
                type: "POST",
                success: function () {
                    swal("Success Booking", "", "success");
                    setTimeout(function(){
                        window.location.reload();
                    }, 1500);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Opppss", "Failed Booking", "error");
                }
            });
        });
    });

    $(document).on("click","#pay",function() {
        var id = $(this).attr('data-id');
        var price;

        $.ajax({
            url: "{{ url('checkpayment') }}",
            data: {'id': id, "_token": "{{ csrf_token() }}"},
            type: "POST",
            dataType: 'json',
            success: function ($data) {
                swal({
                    title: "Are you sure pay for this parking?",
                    text: 'Billed amount: $'+$data['price'],
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#EB0000",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $.ajax({
                        url: "{{ url('paying') }}",
                        data: {'id': id, "_token": "{{ csrf_token() }}"},
                        type: "POST",
                        success: function () {
                            swal("Your Payment Has Been Successfully Received", "", "success");
                            setTimeout(function(){
                                window.location.reload();
                            }, 1500);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Opppss", "Failed Paying", "error");
                        }
                    });
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Opppss", "Failed Paying", "error");
            }
        });


    });
</script>
